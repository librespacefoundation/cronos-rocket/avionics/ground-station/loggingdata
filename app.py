
from flask import Flask,  request, jsonify, make_response
from flask_cors import CORS  
from urllib.parse import unquote
import backend
import datetime

app = Flask(__name__)
CORS(app)

first_date = datetime.datetime.now()

@app.route('/data/<id>', methods=['GET'])
def getData(id):
    return "success"


@app.route('/insert-data', methods=['POST'])
def insertData():    
    time = datetime.datetime.now() 
    line = unquote(request.args.get('data'))
    # print("line: ", line)
    with open("file.txt", "a") as f:
        f.write(str(time) + "[] " + line + "\n")
    # backend.insertData(line, time)

    # print(datetime.datetime.now() - time, flush=True)
    return "success"

@app.route('/insert-data', methods=['GET'])
def write_db():
    global first_date
    prev = first_date
    with open("file.txt", "a") as f:
        for line in f.readline():
            time = line.split('[] ')[0]
            data = line.split('[] ')[1]
            backend.insertData(data, prev, time)
            
if __name__ == '__main__': 
    # backend.printDb()
    app.run(port=5000, debug=True)
    
