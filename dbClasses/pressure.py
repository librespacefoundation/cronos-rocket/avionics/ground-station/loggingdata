import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

from .base import Base 

class Pressure(Base):
    __tablename__ = "pressure"

    id = db.Column(db.Integer, primary_key=True, unique=True)
    timestamp = db.Column(db.DateTime)
    
    runTankPressure = db.Column(db.Float) 
    injectorPressure = db.Column(db.Float) 
    ccPressure = db.Column(db.Float) 

    def __str__(self):
        return f'Pressure -> {self.timestamp}: {self.runTankPressure}, {self.injectorPressure}, {self.ccPressure}'