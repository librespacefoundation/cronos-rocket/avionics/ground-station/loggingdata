import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

from .base import Base 

class Weight(Base):
    __tablename__ = "weight"

    id = db.Column(db.Integer, primary_key=True, unique=True)
    timestamp = db.Column(db.DateTime)
    
    runTankLoadcell = db.Column(db.Float) 
    engine1 = db.Column(db.Float) 
    engine2 = db.Column(db.Float) 
    engine3 = db.Column(db.Float)  
    def __str__(self):
        return f'{self.id} {self.timestamp} {self.engine1} {self.engine2} {self.engine3}'
    