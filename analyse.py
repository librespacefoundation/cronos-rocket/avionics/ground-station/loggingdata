# from re import I
# import sqlalchemy as db
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import session, sessionmaker, relationship, aliased
# from sqlalchemy.exc import OperationalError
# from sqlalchemy.sql import func
# from sqlalchemy.pool import SingletonThreadPool

# from dbClasses.base import Base
# from dbClasses.weight import Weight
# from dbClasses.pressure import Pressure
# from dbClasses.state import State
# from dbClasses.pot import Pot
# import datetime
# import matplotlib.pyplot as plt
# import matplotlib as mpl 

# # mpl.rcParams['figure.dpi'] = 300

# engine = db.create_engine('sqlite:///Results-ColdFlow/data.sqlite', poolclass=SingletonThreadPool) 

# Base.metadata.create_all(bind=engine)
# Session = sessionmaker(bind=engine)

# session = Session()

# progress = [int(float(s[0])) for s in session.query(State.progress).all()] # [92000:102500] 

# start , last = 0,0 
# for i, p in enumerate(progress):
#     if p == 1:
#         start = i 
#     if p == 6:
#         last = i 
# start -= 1000 
# # last += 1000 


# timestamp = session.query(Pressure.timestamp).all()#[start:last]
# pressure_runtank = session.query(Pressure.runTankPressure).all()#[start:last]
# pressure_injector = session.query(Pressure.injectorPressure).all()#[start:last]
# pressure_cc = session.query(Pressure.ccPressure).all()#[start:last]

# print(timestamp[6900][0], timestamp[7700][0])

# with open("pressure.txt", "w") as f:
#     f.write("date, runtank, injector, cc\n")
#     for i in range(len(timestamp)):
#         f.write(str(timestamp[i][0]) + "," + str(pressure_runtank[i][0]) + "," + str(pressure_injector[i][0]) + "," + str(pressure_cc[i][0]) + "\n")
    

# timestamp = session.query(Weight.timestamp).all()[start:last]
# weight_runtank = session.query(Weight.runTankLoadcell).all()[start:last]
# weight_engine1 = session.query(Weight.engine1).all()[start:last]
# weight_engine2 = session.query(Weight.engine2).all()[start:last]
# weight_engine3 = session.query(Weight.engine3).all()[start:last]

# with open("weight.txt", "w") as f:
#     f.write("date, runtank, engine1, engine2, engine3\n")
#     for i in range(len(timestamp)):
#         f.write(str(timestamp[i][0]) + "," + str(weight_runtank[i][0]) + "," + str(weight_engine1[i][0]) + "," + str(weight_engine2[i][0]) + "," + str(weight_engine3[i][0]) + "\n")
    

# timestamp = session.query(Pot.timestamp).all()[start:last]
# fill_pot = session.query(Pot.fillPot).all()[start:last]
# thrust_pot = session.query(Pot.thrustPot).all()[start:last] 
# purge_pot = session.query(Pot.purgePot).all()[start:last]


# with open("pots.txt", "w") as f:
#     f.write("date, fill, thrust, purge\n")
#     for i in range(len(timestamp)):
#         f.write(str(timestamp[i][0]) + "," + str(fill_pot[i][0]) + "," + str(thrust_pot[i][0]) + "," + str(purge_pot[i][0]) + "\n") 
    
# progress = progress[start:last]

# with open("progress.txt", "w") as f:
#     f.write("Total Data: " + str(len(pressure_runtank)) + "\n")
#     f.write("Starting Date: " + str(timestamp[0][0]) + "\n")

#     status = -1
#     for i, p in enumerate(progress):
#         if status != p:
#             if p == 0: 
#                 f.write("Neutral Start: " + str(timestamp[i][0]) + "\n") 
#                 status = p
#             elif p == 1:
#                 f.write("Valve Check Start: " + str(timestamp[i][0]) + "\n")
#                 status = p
#             elif p == 2:
#                 f.write("Fill Start: " + str(timestamp[i][0]) + "\n")
#                 status = p
#             elif p == 3:
#                 f.write("Fire Start: " + str(timestamp[i][0]) + "\n")
#                 status = p
#             elif p == 4:
#                 f.write("Approach Start: " + str(timestamp[i][0]) + "\n")
#                 status = p
#             elif p == 5:
#                 f.write("Abort Start: " + str(timestamp[i][0]) + "\n")
#                 status = p
#             elif p == 6:
#                 f.write("Ready State from: " + str(timestamp[i][0]) + "\n")
#                 status = p

#     f.write("Final Date: " + str(timestamp[len(timestamp)-1][0]) + "\n")

# plt.figure()
# plt.plot(pressure_runtank)
# plt.plot(pressure_injector)
# plt.plot(pressure_cc)
# plt.title('Pressures (bar)')
# plt.legend(["Runtank", "Injector", "CC"])

# plt.figure() #
# plt.plot(weight_runtank) 
# plt.title('Runtank weight (kg)')

# plt.figure() 
# plt.plot(fill_pot)
# plt.plot(thrust_pot)
# plt.plot(purge_pot)
# plt.title('Pots')
# plt.legend(['Fill','Thrust','Purge'])

# plt.show() 

# session.close()

with open("file.txt", "r") as f:
    for line in f.readlines():
        # print(line)
        for i, c in enumerate(line):
            if not (32 <= int(ord(c)) <= 126):
                print(ord(c), i, len(line))
                print(line[(i - 5): (i + 10)])