import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import session, sessionmaker, relationship, aliased
from sqlalchemy.exc import OperationalError
from sqlalchemy.sql import func
from sqlalchemy.pool import SingletonThreadPool

from dbClasses.base import Base
from dbClasses.weight import Weight
from dbClasses.pressure import Pressure
from dbClasses.state import State
from dbClasses.pot import Pot

import datetime 

engine = db.create_engine('sqlite:///data.sqlite', poolclass=SingletonThreadPool) 

Base.metadata.create_all(bind=engine)
Session = sessionmaker(bind=engine)

prev_time = datetime.datetime.now()

def printDb():
    session = Session()
    print(session.query(Weight).all())
    session.close()

def getDataFormat(line):
    line = line.replace(' ', '')

    data = { 
        'lr': [],
        'lc1': [],
        'lc2': [],
        'lc3': [],
        'mf': [],
        'mq': [],
        'mt': [],
        'mr': [],
        'mp': [],
        'p': [], 
        'wt': [],
        'wf': [],
        'wp': [],
        'pr': [],
        'pi': [],
        'pc': []
    }

    line = line.split(',')  

    for obj in line:
        try:
            s = obj.split(':') 
            if s[0] in data and len(s) == 2: 
                data[s[0]].append(float(s[1]))
        except Exception as e:
            print(e)

    max_size = 0
    for d in data:
        max_size = max(max_size, len(data[d]))

    for d in data:
        if len(data[d]) < max_size and len(data[d]) > 0: 
            data[d].extend([data[d][-1] for i in range(max_size - len(data[d]))] )
    return data

def insertData(line, prev_time, time):
    def help(data, var, i):
        try: 
            return data[var][i] 
        except Exception as e:
            return None #

    data = getDataFormat(line) 

    session = Session()
    #   (datetime -50ms) ... . datetime

    diff_time = time - prev_time
    
    
    size = 0
    for d in data:
        size = max(size, len(data[d]))
    diff_time = diff_time / (size + 1)

    prev_time = time
    prev_time_ = prev_time

    for i in range(size):
        t = prev_time_ +  diff_time * (i+1)
        p = Pressure(
                timestamp=t,
                runTankPressure=help(data, 'pr', i), 
                injectorPressure=help(data, 'pi', i),
                ccPressure=help(data, 'pc', i)
            ) 
        session.add(p) 

        session.add( 
            Weight(
                timestamp=t,
                runTankLoadcell=help(data, 'lr', i), 
                engine1=help(data, 'lc1', i),
                engine2=help(data, 'lc2', i),
                engine3=help(data, 'lc3', i),
            )
        ) 
        session.add( 
            State(
                timestamp=t,
                thrustValveState=help(data, 'mt', i), 
                fillValveState=help(data, 'mf', i),
                qrValveState=help(data, 'mq', i),
                purgeValveState=help(data, 'mp', i), 
                reliefValveState=help(data, 'mr', i),
                progress=help(data, 'p', i)
            )
        )  
        session.add( 
            Pot(
                timestamp=t,
                thrustPot=help(data, 'wt', i), 
                fillPot=help(data, 'wf', i),
                purgePot=help(data, 'wp', i),
            )
        )
        
    session.commit()
        
    session.close()
