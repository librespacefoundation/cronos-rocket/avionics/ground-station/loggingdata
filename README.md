# LoggingData

This is an api in python flask that listens to port 5000 at http://localhost:5000/ and will receive the incoming data of the RMC project and store them in a MySQL database

Execute `gunicorn --bind 0.0.0.0:5000 --workers=2 --access-logfile - app:app` after installing the packages
